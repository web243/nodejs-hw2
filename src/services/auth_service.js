const jwt = require('jsonwebtoken');
const {User} = require('../models/user_model');
const {InvalidCredentialsError} = require('../utils/errors');
const {getUserByUsername} = require('./../services/users_service');
const {checkIfPasswordsMatch,
    encryptPassword} = require('./password_processing');

const getTokenForUser = (user) => {
    const SECRET = process.env.TOKEN_SECRET;
    return jwt.sign({
        _id: user._id,
        username: user.username,
    }, SECRET);
};

const validateRegistrationData = ({username, password}) => {
    if (!username) {
        throw new InvalidCredentialsError('Username must be provided');
    }
    if (!password) {
        throw new InvalidCredentialsError('Password must be provided');
    }
};

const getUserByCredentials = async (username, password) => {
    const user = await getUserByUsername(username);
    await checkIfPasswordsMatch(password, user.password);
    return user;
};

const register = async (username, password) => {
    validateRegistrationData({username, password});
    const passwordEncrypted = await encryptPassword(password);
    try {
        const user = new User({
            username,
            password: passwordEncrypted,
        });
        await user.save();
    } catch (e) {
        throw new InvalidCredentialsError('This username is already taken');
    }
};

const login = async (username, password) => {
    const user = await getUserByCredentials(username, password);
    return getTokenForUser(user);
};

module.exports = {
    register,
    login,
};
