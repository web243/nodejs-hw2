const {User} = require('../models/user_model');
const {InvalidCredentialsError} = require('../utils/errors');
const {checkIfPasswordsMatch,
    encryptPassword,
    validatePassword,
} = require('./password_processing');

const getUserById = (id) => {
    return User.findOne({_id: id});
};

const getUserByUsername = async (username) => {
    const user = await User.findOne({username});
    if (!user) {
        throw new InvalidCredentialsError('Invalid username or password');
    }
    return user;
};

const deleteUserById = async (id) => {
    return await User.deleteOne({_id: id});
};

const changePasswordForUser = async (id, oldPassword, newPassword) => {
    validatePassword(oldPassword);
    validatePassword(newPassword);
    const user = await getUserById(id);
    await checkIfPasswordsMatch(oldPassword, user.password);
    const passwordEncrypted = await encryptPassword(newPassword);
    return await User.updateOne({_id: id}, {password: passwordEncrypted});
};

module.exports = {
    getUserById,
    getUserByUsername,
    deleteUserById,
    changePasswordForUser,
};
