const bcrypt = require('bcrypt');
const {InvalidCredentialsError} = require('../utils/errors');

const checkIfPasswordsMatch = async (plainPassword, hash) => {
    const passwordMatches = await bcrypt.compare(plainPassword, hash);
    if (!passwordMatches) {
        throw new InvalidCredentialsError('Invalid data');
    }
};

const validatePassword = (password) => {
    if (!password) {
        throw new InvalidCredentialsError('Please, enter a password');
    }
};

const encryptPassword = async (password) => {
    const saltRounds = 10;
    return await bcrypt.hash(password, saltRounds);
};

module.exports = {
    checkIfPasswordsMatch,
    encryptPassword,
    validatePassword,
};
