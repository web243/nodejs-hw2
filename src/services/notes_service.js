const {InvalidIdError} = require('../utils/errors');
const {Note} = require('../models/note_model');
const ObjectId = require('mongoose').Types.ObjectId;

const validateIds = (...ids) => {
    ids.forEach((id) => {
        if (!ObjectId.isValid(id)) {
            throw new InvalidIdError();
        }
    });
};

const getAllNotesForUser = async (userId, offset = 0, limit = 0) => {
    return await Note.find({userId}, {
        limit,
        skip: offset,
    });
};

const getNoteByIdForUser = async (noteId, userId) => {
    validateIds(noteId, userId);
    return Note.findOne({_id: noteId, userId});
};

const updateNoteByIdForUser = async (noteId, userId, text) => {
    validateIds(noteId, userId);
    return Note.updateOne({_id: noteId, userId}, {text});
};

const changeNoteCheckForUser = async (noteId, userId) => {
    validateIds(noteId, userId);
    const note = await Note.findOne({_id: noteId, userId});
    const newCheckState = !note.completed;
    return Note.updateOne({_id: noteId, userId}, {completed: newCheckState});
};

const deleteNoteByIdForUser = async (noteId, userId) => {
    validateIds(noteId, userId);
    return Note.deleteOne({_id: noteId, userId});
};

const addNoteToUser = async (userId, text) => {
    const note = new Note({userId, text});
    await note.save();
};

const getAllUserNotesCount = async (userId) => {
    return Note.count({userId});
};

module.exports = {
    getAllNotesForUser,
    addNoteToUser,
    getNoteByIdForUser,
    updateNoteByIdForUser,
    changeNoteCheckForUser,
    deleteNoteByIdForUser,
    getAllUserNotesCount,
};
