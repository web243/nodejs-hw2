class NotesProjectError extends Error {
    constructor(message) {
        super(message);
        this.status = 500;
    }
}

class InvalidCredentialsError extends NotesProjectError {
    constructor(message) {
        super(message);
        this.status = 400;
    }
}

class InvalidIdError extends NotesProjectError {
    constructor(message = 'Invalid ID. Nothing found') {
        super(message);
        this.status = 400;
    }
}

module.exports = {
    NotesProjectError,
    InvalidCredentialsError,
    InvalidIdError,
};
