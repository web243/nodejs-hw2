function getResponseStatus(error) {
    return error.status ? error.status : 500;
}

module.exports = {
    getResponseStatus,
};
