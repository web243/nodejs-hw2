require('dotenv').config();
const express = require('express');
const mongoose = require('mongoose');
const morgan = require('morgan');
const cors = require('cors');
const {authRouter} = require('./controllers/auth_controller');
const {usersRouter} = require('./controllers/users_controller');
const {notesRouter} = require('./controllers/notes_controller');
const {authMiddleware} = require('./middleware/auth_middleware');

const app = express();
const PORT = process.env.PORT;
const DB_URI = process.env.DB_URI;
const dbOptions = {
    useNewUrlParser: true,
    useUnifiedTopology: true,
    useFindAndModify: false,
    useCreateIndex: true,
};

app.use(morgan('short'));
app.use(cors()); // Enable All CORS Requests
app.use(express.json());
app.use('/api/auth', authRouter);

app.use(authMiddleware);
app.use('/api/users', usersRouter);
app.use('/api/notes', notesRouter);

app.use((req, res, next) => {
    res.status(404).json({message: 'Not found'});
});
app.use((err, req, res, next) => {
    res.status(err.status ? err.status : 500).json({message: err.message});
});

const start = async () => {
    try {
        await mongoose.connect(DB_URI, dbOptions);
        app.listen(PORT, () => {
            console.log(`Server is listening on port ${PORT}`);
        });
    } catch (e) {
        console.error(e);
    }
};

start();
