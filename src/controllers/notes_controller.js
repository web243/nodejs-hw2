const express = require('express');
const {
    getAllNotesForUser,
    addNoteToUser,
    getNoteByIdForUser,
    updateNoteByIdForUser,
    changeNoteCheckForUser,
    deleteNoteByIdForUser,
    getAllUserNotesCount,
} = require('./../services/notes_service');
const {asyncWrapper} = require('./../utils/async_wrapper');

// eslint-disable-next-line new-cap
const router = express.Router();

const getZeroIfUndefined = (possibleUndefined) => {
    return possibleUndefined ? possibleUndefined : 0;
};

const getUserIfFromReq = (req) => {
    return req.user._id;
};

router.get('/', asyncWrapper(async (req, res) => {
    const {offset, limit} = req.query;
    const userId = getUserIfFromReq(req);
    const notes = await getAllNotesForUser(userId,
        offset, limit);
    const notesCount = await getAllUserNotesCount(userId);
    res.status(200).json({
        offset: getZeroIfUndefined(offset),
        limit: getZeroIfUndefined(limit),
        count: notesCount,
        notes,
    });
}));

router.post('/', asyncWrapper(async (req, res) => {
    const text = req.body.text;
    const userId = getUserIfFromReq(req);
    await addNoteToUser(userId, text);
    res.status(200).json({message: 'Success'});
}));

router.get('/:id', asyncWrapper(async (req, res) => {
    const userId = getUserIfFromReq(req);
    const note = await getNoteByIdForUser(req.params.id, userId);
    res.status(200).json({note});
}));

router.put('/:id', asyncWrapper(async (req, res) => {
    const userId = getUserIfFromReq(req);
    await updateNoteByIdForUser(req.params.id, userId, req.body.text);
    res.status(200).json({message: 'Success'});
}));

router.patch('/:id', asyncWrapper(async (req, res) => {
    const userId = getUserIfFromReq(req);
    await changeNoteCheckForUser(req.params.id, userId);
    res.status(200).json({message: 'Success'});
}));

router.delete('/:id', asyncWrapper(async (req, res) => {
    const userId = getUserIfFromReq(req);
    await deleteNoteByIdForUser(req.params.id, userId);
    res.status(200).json({message: 'Success'});
}));


module.exports = {
    notesRouter: router,
};
