const express = require('express');
const {getUserById,
    deleteUserById,
    changePasswordForUser,
} = require('./../services/users_service');
const {asyncWrapper} = require('./../utils/async_wrapper');

// eslint-disable-next-line new-cap
const router = express.Router();

router.get('/me', asyncWrapper(async (req, res) => {
    const user = await getUserById(req.user._id);
    res.status(200).json({user});
}));

router.delete('/me', asyncWrapper(async (req, res) => {
    await deleteUserById(req.user._id);
    res.status(200).json({message: 'Success'});
}));

router.patch('/me', asyncWrapper(async (req, res) => {
    await changePasswordForUser(req.user._id,
        req.body.oldPassword,
        req.body.newPassword);
    res.status(200).json({message: 'Success'});
}));

module.exports = {
    usersRouter: router,
};
