const express = require('express');
const {register, login} = require('../services/auth_service');
const {asyncWrapper} = require('./../utils/async_wrapper');

// eslint-disable-next-line new-cap
const router = express.Router();

router.post('/register', asyncWrapper(async (req, res) => {
    const {username, password} = req.body;
    await register(username, password);
    res.status(200).json({message: 'Success'});
}));

router.post('/login', asyncWrapper(async (req, res) => {
    console.log(req.body);
    const {username, password} = req.body;
    const token = await login(username, password);
    res.status(200).json({
        message: 'Success',
        jwt_token: token,
    });
}));

module.exports = {
    authRouter: router,
};
